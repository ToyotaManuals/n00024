<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="../../../system/css/global.css">
<link rel="stylesheet" type="text/css" href="../css/contents.css">
<link rel="stylesheet" type="text/css" href="../../../system/css/facebox.css">
<link rel="stylesheet" type="text/css" href="../css/print.css" id="print_css">
<script type="text/javascript" src="../../../system/js/util.js?cntl=Contents" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_const.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_message.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/use.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/contents.js" charset="UTF-8"></script>
<title>Toyota Service Information</title>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
<div id="body">
<div id="contents">
<div id="contentsBody" class="fontEn">
<div class="globalInfo">
<span class="globalPubNo">NM3090E</span>
<span class="globalServcat">_52</span>
<span class="globalServcatName">Drivetrain</span>
<span class="globalSection">_046774</span>
<span class="globalSectionName">K114 MULTIDRIVE / CVT</span>
<span class="globalTitle">_0220995</span>
<span class="globalTitleName">CVT SYSTEM</span>
<span class="globalSubTitle">DE</span>
<span class="globalSubTitleName">DETAILS</span>
<span class="globalCategory">F</span>
</div>
<h1>K114 MULTIDRIVE / CVT&nbsp;&nbsp;CVT SYSTEM&nbsp;&nbsp;DETAILS&nbsp;&nbsp;TRANSMISSION CONTROL SWITCH&nbsp;&nbsp;</h1>
<br>
<div id="NM100000000Q7YT_z0" class="category no00">
<div class="content5">
<div class="step1">
<p class="step1"><span class="titleText">CONSTRUCTION
</span></p>
<br>
<div class="step2">
<div class="step2Item step2HasFigure">
<div class="step2Head">a.</div>
<div class="step2Body">
<p>The transmission control switch is installed inside the shift lever assembly to detect whether the shift lever is in M (S signal) and the operating conditions of the shift lever "+" (upshift) or "-" (downshift).
</p>
<div class="figure">
<div class="cPattern">
<div class="graphic">
<img src="../img/png/X129382C01.png" alt="X129382C01" title="X129382C01">
<div class="indicateinfo">
<span class="line">
<span class="points">2.531,1.333 2.979,1.573</span>
<span class="whiteedge">true</span>
</span>
<span class="line">
<span class="points">3.083,3.271 3.594,2.417</span>
<span class="whiteedge">true</span>
</span>
<span class="caption">
<span class="points">2.354,1.198 2.667,1.396</span>
<span class="captionSize">0.313,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*1</span>
</span>
<span class="caption">
<span class="points">2.906,3.292 3.219,3.49</span>
<span class="captionSize">0.313,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*2</span>
</span>
</div>
</div>
<table summary="caption-table">
<colgroup>
<col style="width:10%">
<col style="width:39%">
<col style="width:10%">
<col style="width:39%">
</colgroup>
<tbody>
<tr>
<td class="alcenter">*1
</td>
<td class="alleft">Shift Lever Assembly (Transmission Floor Shift Assembly)
</td>
<td class="alcenter">*2
</td>
<td class="alleft">Transmission Control Switch
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
<br>
</div>
</div>
</div>
<br>
<div class="step2">
<div class="step2Item step2HasFigure">
<div class="step2Head">b.</div>
<div class="step2Body">
<p>The transmission control switch sends the S, SFTU and SFTD signals to the ECM. The ECM also sends the selected shift range position signal to the combination meter assembly via CAN.
</p>
<div class="figure">
<div class="cPattern">
<div class="graphic">
<img src="../img/png/X129383E02.png" alt="X129383E02" title="X129383E02">
<div class="indicateinfo">
<span class="caption">
<span class="points">4.74,1.667 6.479,1.958</span>
<span class="captionSize">1.74,0.292</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">CAN (Bus 2*1 or V Bus*2)</span>
</span>
<span class="caption">
<span class="points">4.031,1.198 4.49,1.396</span>
<span class="captionSize">0.458,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">ECM</span>
</span>
<span class="caption">
<span class="points">5.271,0.323 6.458,1.01</span>
<span class="captionSize">1.188,0.688</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Combination Meter Assembly</span>
</span>
<span class="caption">
<span class="points">3.458,0.813 3.625,1.01</span>
<span class="captionSize">0.167,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">S</span>
</span>
<span class="caption">
<span class="points">3.219,1.26 3.646,1.469</span>
<span class="captionSize">0.427,0.208</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">SFTU</span>
</span>
<span class="caption">
<span class="points">3.229,1.729 3.646,1.927</span>
<span class="captionSize">0.417,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">SFTD</span>
</span>
<span class="caption">
<span class="points">1.271,2.24 3.49,2.708</span>
<span class="captionSize">2.219,0.469</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">Transmission Control Switch</span>
</span>
<span class="caption">
<span class="points">0.292,0.667 1.323,1.135</span>
<span class="captionSize">1.031,0.469</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">From IG1 Relay</span>
</span>
<span class="caption">
<span class="points">4.021,2.208 7.094,2.458</span>
<span class="captionSize">3.073,0.25</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*1: Models with automatic air conditioning system</span>
</span>
<span class="caption">
<span class="points">4.031,2.469 7.021,2.719</span>
<span class="captionSize">2.99,0.25</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*2: Models with manual air conditioning system</span>
</span>
</div>
</div>
</div>
</div>
<br>
</div>
</div>
</div>
<br>
</div>
<br>
</div>
</div>
</div>
</div>
<div id="footer"></div>
</div>
</div>
</body>
</html>
