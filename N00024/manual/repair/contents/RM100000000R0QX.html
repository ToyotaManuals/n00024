<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<link rel="stylesheet" type="text/css" href="../../../system/css/global.css">
<link rel="stylesheet" type="text/css" href="../css/contents.css">
<link rel="stylesheet" type="text/css" href="../../../system/css/facebox.css">
<link rel="stylesheet" type="text/css" href="../css/print.css" id="print_css">
<script type="text/javascript" src="../../../system/js/util.js?cntl=Contents" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_const.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/dict_message.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/use.js" charset="UTF-8"></script>
<script type="text/javascript" src="../../../system/js/contents.js" charset="UTF-8"></script>
<title>Toyota Service Information</title>
</head>
<body>
<div id="wrapper">
<div id="header"></div>
<div id="body">
<div id="contents">
<div id="contentsBody" class="fontEn">
<div class="globalInfo">
<span class="globalPubNo">RM3090E</span>
<span class="globalServcat">_55</span>
<span class="globalServcatName">Steering</span>
<span class="globalSection">_008773</span>
<span class="globalSectionName">STEERING COLUMN</span>
<span class="globalTitle">_0049279</span>
<span class="globalTitleName">STEERING LOCK SYSTEM</span>
<span class="globalCategory">R</span>
</div>
<h1>STEERING COLUMN&nbsp;&nbsp;STEERING LOCK SYSTEM&nbsp;&nbsp;PARTS LOCATION&nbsp;&nbsp;</h1>
<br>
<div id="RM100000000R0QX_06" class="category no04">
<h2>ILLUSTRATION</h2>
<div class="content2">
<div class="figure">
<div class="cPattern">
<div class="graphic">
<img src="../img/png/C278716C01.png" alt="C278716C01" title="C278716C01">
<div class="indicateinfo">
<span class="line">
<span class="points">3.594,5.021 4.573,6.563</span>
<span class="points">4.573,6.563 4.573,6.729</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">3.073,4.771 0.969,3.354</span>
<span class="points">0.969,3.354 0.771,3.344</span>
<span class="whiteedge">false</span>
</span>
<span class="caption">
<span class="points">0.479,3.26 0.792,3.417</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">*1</span>
</span>
<span class="caption">
<span class="points">4.521,6.802 4.833,6.958</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">*2</span>
</span>
<span class="caption">
<span class="points">4.521,6.969 4.833,7.125</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">- *3</span>
</span>
<span class="caption">
<span class="points">4.521,7.115 4.833,7.271</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">- *4</span>
</span>
<span class="caption">
<span class="points">4.521,7.281 4.833,7.438</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="captionText">- *5</span>
</span>
</div>
</div>
<table summary="caption-table">
<colgroup>
<col style="width:10%">
<col style="width:39%">
<col style="width:10%">
<col style="width:39%">
</colgroup>
<tbody>
<tr>
<td class="alcenter">*1
</td>
<td>ECM
</td>
<td class="alcenter">*2
</td>
<td>ENGINE ROOM RELAY BLOCK AND JUNCTION BLOCK ASSEMBLY
</td>
</tr>
<tr>
<td class="alcenter">*3
</td>
<td>IG2 RELAY
</td>
<td class="alcenter">*4
</td>
<td>IG2 FUSE
</td>
</tr>
<tr>
<td class="alcenter">*5
</td>
<td>STRG LOCK FUSE
</td>
<td class="alcenter">-
</td>
<td class="alcenter">-
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
<br>
</div>
</div>
<div id="RM100000000R0QX_05" class="category no04">
<h2>ILLUSTRATION</h2>
<div class="content2">
<div class="figure">
<div class="cPattern">
<div class="graphic">
<img src="../img/png/C278717C01.png" alt="C278717C01" title="C278717C01">
<div class="indicateinfo">
<span class="line">
<span class="points">1.583,4.167 1.167,3.833</span>
<span class="points">1.167,3.833 1,3.833</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">3.333,3.833 3.583,2.25</span>
<span class="points">3.583,2.25 3.75,2.25</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">1.667,4.719 1.25,5.25</span>
<span class="points">1.25,5.25 1.083,5.25</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">2.333,3.583 2.5,2.333</span>
<span class="points">2.5,2.333 2.667,2.333</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">4.167,5.25 4.75,6.167</span>
<span class="points">4.75,6.167 4.917,6.167</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">2.292,1.323 3.146,0.438</span>
<span class="points">3.146,0.438 3.313,0.438</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">1.771,7.5 2.26,7.5</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">4.281,7.479 4.781,7.458</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">0.74,4.438 1.625,4.406</span>
<span class="whiteedge">false</span>
</span>
<span class="caption">
<span class="points">0.219,0.271 0.531,0.427</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*A</span>
</span>
<span class="caption">
<span class="points">2.531,7.448 2.844,7.604</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*B</span>
</span>
<span class="caption">
<span class="points">5.156,6.104 5.469,6.26</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*C</span>
</span>
<span class="caption">
<span class="points">3.844,2.198 4.156,2.354</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*1</span>
</span>
<span class="caption">
<span class="points">2.75,2.271 3.063,2.427</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*2</span>
</span>
<span class="caption">
<span class="points">0.844,3.771 1.156,3.927</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*3</span>
</span>
<span class="caption">
<span class="points">3.365,0.365 3.677,0.521</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*4</span>
</span>
<span class="caption">
<span class="points">0.927,5.177 1.24,5.333</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*5</span>
</span>
<span class="caption">
<span class="points">4.99,6.104 5.302,6.26</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*6</span>
</span>
<span class="caption">
<span class="points">2.344,7.438 2.656,7.594</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*7</span>
</span>
<span class="caption">
<span class="points">4.875,7.396 5.188,7.552</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*8</span>
</span>
<span class="caption">
<span class="points">0.552,6.958 0.865,7.115</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*a</span>
</span>
<span class="caption">
<span class="points">0.563,4.375 0.875,4.573</span>
<span class="captionSize">0.313,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*9</span>
</span>
<span class="caption">
<span class="points">0.573,4.583 0.917,4.76</span>
<span class="captionSize">0.344,0.177</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">- *10</span>
</span>
</div>
</div>
<table summary="caption-table">
<colgroup>
<col style="width:10%">
<col style="width:39%">
<col style="width:10%">
<col style="width:39%">
</colgroup>
<tbody>
<tr>
<td class="alcenter">*A
</td>
<td>for LHD
</td>
<td class="alcenter">*B
</td>
<td>w/ ID Code Box (Immobiliser Code ECU)
</td>
</tr>
<tr>
<td class="alcenter">*C
</td>
<td>except Manual Transaxle
</td>
<td class="alcenter">-
</td>
<td class="alcenter">-
</td>
</tr>
<tr>
<td class="alcenter">*1
</td>
<td><a class="hiddenCallout" href="javascript:void(0);" rel="201212,201311">
ENGINE SWITCH
<span class="invisible">_51,_008740,_0060095,1AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008741,_0049819,2AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008411,_0046983,3ZR-FE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008739,_0049808,2AD-FHV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008393,_0046848,3ZR-FAE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008402,_0046914,2AR-FE STARTING/ENGINE SWITCH</span></a>
<a class="hiddenCallout" href="javascript:void(0);" rel="201311,201410">
ENGINE SWITCH
<span class="invisible">_51,_008740,_0060095,1AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008741,_0049819,2AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008411,_0046983,3ZR-FE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008739,_0049808,2AD-FHV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008393,_0046848,3ZR-FAE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008402,_0046914,2AR-FE STARTING/ENGINE SWITCH</span></a>
<a class="hiddenCallout" href="javascript:void(0);" rel="201410,201504">
ENGINE SWITCH
<span class="invisible">_51,_008740,_0060095,1AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008741,_0049819,2AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008411,_0046983,3ZR-FE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008739,_0049808,2AD-FHV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008393,_0046848,3ZR-FAE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008402,_0046914,2AR-FE STARTING/ENGINE SWITCH</span></a>
<a class="hiddenCallout" href="javascript:void(0);" rel="201504,201510">
ENGINE SWITCH
<span class="invisible">_51,_008740,_0060095,1AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008741,_0049819,2AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008411,_0046983,3ZR-FE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008739,_0049808,2AD-FHV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008393,_0046848,3ZR-FAE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008402,_0046914,2AR-FE STARTING/ENGINE SWITCH</span></a>
<a class="hiddenCallout" href="javascript:void(0);" rel="201510,201608">
ENGINE SWITCH
<span class="invisible">_51,_008741,_0049819,2AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008411,_0046983,3ZR-FE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_044379,_0219542,2WW STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008739,_0049808,2AD-FHV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008393,_0046848,3ZR-FAE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008402,_0046914,2AR-FE STARTING/ENGINE SWITCH</span></a>
<a class="hiddenCallout" href="javascript:void(0);" rel="201608,999999">
ENGINE SWITCH
<span class="invisible">_51,_008741,_0049819,2AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008411,_0046983,3ZR-FE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_044379,_0219542,2WW STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008739,_0049808,2AD-FHV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008393,_0046848,3ZR-FAE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008402,_0046914,2AR-FE STARTING/ENGINE SWITCH</span></a>
</td>
<td class="alcenter">*2
</td>
<td class="alleft">COMBINATION METER ASSEMBLY
</td>
</tr>
<tr>
<td class="alcenter">*3
</td>
<td>MAIN BODY ECU (MULTIPLEX NETWORK BODY ECU)
</td>
<td class="alcenter">*4
</td>
<td class="alleft">STEERING LOCK ECU (STEERING LOCK ACTUATOR ASSEMBLY)
</td>
</tr>
<tr>
<td class="alcenter">*5
</td>
<td>DLC3
</td>
<td class="alcenter">*6
</td>
<td>SHIFT LOCK CONTROL ECU (LOWER SHIFT LEVER ASSEMBLY)
</td>
</tr>
<tr>
<td class="alcenter">*7
</td>
<td>ID CODE BOX (IMMOBILISER CODE ECU)
</td>
<td class="alcenter">*8
</td>
<td>CERTIFICATION ECU (SMART KEY ECU ASSEMBLY)
</td>
</tr>
<tr>
<td class="alcenter">*9
</td>
<td>INSTRUMENT PANEL JUNCTION BLOCK ASSEMBLY
</td>
<td class="alcenter">*10
</td>
<td>IGN FUSE
</td>
</tr>
<tr>
<td class="alcenter">*a
</td>
<td>Refer to the Service Bulletin for the installation position of the parts.
</td>
<td class="alcenter">-
</td>
<td class="alcenter">-
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
<br>
</div>
</div>
<div id="RM100000000R0QX_04" class="category no04">
<h2>ILLUSTRATION</h2>
<div class="content2">
<div class="figure">
<div class="cPattern">
<div class="graphic">
<img src="../img/png/C278718C01.png" alt="C278718C01" title="C278718C01">
<div class="indicateinfo">
<span class="line">
<span class="points">4.323,3.021 4.833,1.833</span>
<span class="points">4.833,1.833 5,1.833</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">4.917,3.604 5.583,2.417</span>
<span class="points">5.583,2.417 5.75,2.417</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">4.583,4 5.333,4.25</span>
<span class="points">5.333,4.25 5.5,4.25</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">3.396,1.396 2.833,0.76</span>
<span class="points">2.833,0.76 2.635,0.76</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">3.917,5.25 4.417,6.333</span>
<span class="points">4.417,6.333 4.583,6.333</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">1.417,4.583 1.083,5</span>
<span class="points">1.083,5 0.917,5</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">1.781,7.5 2.292,7.5</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">4.281,7.438 4.656,7.448</span>
<span class="whiteedge">false</span>
</span>
<span class="line">
<span class="points">0.75,2.917 0.948,2.917</span>
<span class="points">0.948,2.917 1.521,4.125</span>
<span class="whiteedge">false</span>
</span>
<span class="caption">
<span class="points">0.229,0.198 0.542,0.354</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*A</span>
</span>
<span class="caption">
<span class="points">2.583,7.438 2.896,7.594</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*B</span>
</span>
<span class="caption">
<span class="points">4.875,6.26 5.188,6.417</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*C</span>
</span>
<span class="caption">
<span class="points">5.813,2.354 6.125,2.51</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*1</span>
</span>
<span class="caption">
<span class="points">5.073,1.771 5.385,1.927</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*2</span>
</span>
<span class="caption">
<span class="points">0.74,4.927 1.052,5.083</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*3</span>
</span>
<span class="caption">
<span class="points">2.448,0.688 2.76,0.844</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*4</span>
</span>
<span class="caption">
<span class="points">5.552,4.177 5.865,4.333</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*5</span>
</span>
<span class="caption">
<span class="points">4.677,6.25 4.99,6.406</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*6</span>
</span>
<span class="caption">
<span class="points">2.396,7.427 2.708,7.583</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*7</span>
</span>
<span class="caption">
<span class="points">4.729,7.396 5.042,7.552</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*8</span>
</span>
<span class="caption">
<span class="points">0.583,7 0.896,7.156</span>
<span class="captionSize">0.313,0.156</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*a</span>
</span>
<span class="caption">
<span class="points">0.552,2.844 0.865,3.042</span>
<span class="captionSize">0.313,0.198</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">*9</span>
</span>
<span class="caption">
<span class="points">0.552,3.052 0.927,3.219</span>
<span class="captionSize">0.375,0.167</span>
<span class="fontsize">10</span>
<span class="filled">false</span>
<span class="captionText">- *10</span>
</span>
</div>
</div>
<table summary="caption-table">
<colgroup>
<col style="width:10%">
<col style="width:39%">
<col style="width:10%">
<col style="width:39%">
</colgroup>
<tbody>
<tr>
<td class="alcenter">*A
</td>
<td>for RHD
</td>
<td class="alcenter">*B
</td>
<td>w/ ID Code Box (Immobiliser Code ECU)
</td>
</tr>
<tr>
<td class="alcenter">*C
</td>
<td>except Manual Transaxle
</td>
<td class="alcenter">-
</td>
<td class="alcenter">-
</td>
</tr>
<tr>
<td class="alcenter">*1
</td>
<td><a class="hiddenCallout" href="javascript:void(0);" rel="201212,201311">
ENGINE SWITCH
<span class="invisible">_51,_008740,_0060095,1AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008741,_0049819,2AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008411,_0046983,3ZR-FE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008739,_0049808,2AD-FHV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008393,_0046848,3ZR-FAE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008402,_0046914,2AR-FE STARTING/ENGINE SWITCH</span></a>
<a class="hiddenCallout" href="javascript:void(0);" rel="201311,201410">
ENGINE SWITCH
<span class="invisible">_51,_008740,_0060095,1AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008741,_0049819,2AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008411,_0046983,3ZR-FE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008739,_0049808,2AD-FHV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008393,_0046848,3ZR-FAE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008402,_0046914,2AR-FE STARTING/ENGINE SWITCH</span></a>
<a class="hiddenCallout" href="javascript:void(0);" rel="201410,201504">
ENGINE SWITCH
<span class="invisible">_51,_008740,_0060095,1AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008741,_0049819,2AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008411,_0046983,3ZR-FE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008739,_0049808,2AD-FHV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008393,_0046848,3ZR-FAE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008402,_0046914,2AR-FE STARTING/ENGINE SWITCH</span></a>
<a class="hiddenCallout" href="javascript:void(0);" rel="201504,201510">
ENGINE SWITCH
<span class="invisible">_51,_008740,_0060095,1AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008741,_0049819,2AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008411,_0046983,3ZR-FE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008739,_0049808,2AD-FHV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008393,_0046848,3ZR-FAE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008402,_0046914,2AR-FE STARTING/ENGINE SWITCH</span></a>
<a class="hiddenCallout" href="javascript:void(0);" rel="201510,201608">
ENGINE SWITCH
<span class="invisible">_51,_008741,_0049819,2AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008411,_0046983,3ZR-FE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_044379,_0219542,2WW STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008739,_0049808,2AD-FHV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008393,_0046848,3ZR-FAE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008402,_0046914,2AR-FE STARTING/ENGINE SWITCH</span></a>
<a class="hiddenCallout" href="javascript:void(0);" rel="201608,999999">
ENGINE SWITCH
<span class="invisible">_51,_008741,_0049819,2AD-FTV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008411,_0046983,3ZR-FE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_044379,_0219542,2WW STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008739,_0049808,2AD-FHV STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008393,_0046848,3ZR-FAE STARTING/ENGINE SWITCH</span><span class="invisible">_51,_008402,_0046914,2AR-FE STARTING/ENGINE SWITCH</span></a>
</td>
<td class="alcenter">*2
</td>
<td>COMBINATION METER ASSEMBLY
</td>
</tr>
<tr>
<td class="alcenter">*3
</td>
<td class="alleft">MAIN BODY ECU (MULTIPLEX NETWORK BODY ECU)
</td>
<td class="alcenter">*4
</td>
<td class="alleft">STEERING LOCK ECU (STEERING LOCK ACTUATOR ASSEMBLY)
</td>
</tr>
<tr>
<td class="alcenter">*5
</td>
<td class="alleft">DLC3
</td>
<td class="alcenter">*6
</td>
<td class="alleft">SHIFT LOCK CONTROL ECU (LOWER SHIFT LEVER ASSEMBLY)
</td>
</tr>
<tr>
<td class="alcenter">*7
</td>
<td class="alleft">ID CODE BOX (IMMOBILISER CODE ECU)
</td>
<td class="alcenter">*8
</td>
<td class="alleft">CERTIFICATION ECU (SMART KEY ECU ASSEMBLY)
</td>
</tr>
<tr>
<td class="alcenter">*9
</td>
<td>INSTRUMENT PANEL JUNCTION BLOCK ASSEMBLY
</td>
<td class="alcenter">*10
</td>
<td>IGN FUSE
</td>
</tr>
<tr>
<td class="alcenter">*a
</td>
<td class="alleft">Refer to the Service Bulletin for the installation position of the parts.
</td>
<td class="alcenter">-
</td>
<td class="alcenter">-
</td>
</tr>
</tbody>
</table>
<br>
</div>
</div>
<br>
</div>
</div>
</div>
</div>
<div id="footer"></div>
</div>
</div>
</body>
</html>
